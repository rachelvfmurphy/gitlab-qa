# frozen_string_literal: true

describe Gitlab::QA::Report::ResultsInIssues do
  describe '#invoke!' do
    let(:project) { 'valid-project' }
    let(:test_file_full) { 'qa/specs/features/browser_ui/stage/test_spec.rb' }
    let(:test_file_partial) { 'browser_ui/stage/test_spec.rb' }

    let(:title) { "#{test_file_partial} | test-name " }
    let(:description) { "\n\n### DO NOT EDIT BELOW THIS LINE\n\nActive and historical test results:\n\nhttp://issue.url/-/issues/920" }
    let(:testcase_labels) { ['Quality', 'devops::stage', 'status::automated'] }
    let(:issue_labels) { testcase_labels + ['Testcase Linked'] }
    let(:testcase) { Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title, :description).new('http://testcase.url', testcase_labels, 1, 'test_case', 'opened', title, description) }
    let(:issue) { Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title).new('http://issue.url', issue_labels, 0, 'issue', 'opened', title) }

    context 'with valid input' do
      let(:test_file) { 'file.xml' }
      let(:test_data) { %(<testcase name="test-name" file="#{test_file_full}"/>) }

      subject { described_class.new(token: 'token', input_files: 'files', project: project) }

      before do
        allow(subject).to receive(:assert_input_files!)
        allow(subject.__send__(:gitlab)).to receive(:assert_user_permission!)
        allow(::Dir).to receive(:glob).and_return([test_file])
        allow(::File).to receive(:read).with(test_file).and_return(test_data)
      end

      context 'when a test case exists for a given test but is not linked' do
        let(:search_response) { Struct.new(:auto_paginate).new([testcase]) }

        it 'finds the test case by searching by the test file and name' do
          expect(::Gitlab).to receive(:issues)
            .with(anything, { search: %("#{test_file_partial}" "test-name") })
            .and_return(search_response)

          expect(subject).to receive(:find_linked_results_issue_by_iid).with(testcase, anything).and_return(issue)
          expect(subject).to receive(:update_labels).twice
          expect(subject).to receive(:note_status)

          expect { subject.invoke! }.to output.to_stdout
        end
      end

      context 'when a test case does not exist for a given test' do
        it 'creates a new test case' do
          expect(subject).to receive(:find_testcase).with(anything).and_return(false)
          expect(::Gitlab).to receive(:create_issue).with(anything, anything, anything).and_return(testcase)
          expect(subject).to receive(:find_linked_results_issue_by_iid).with(testcase, anything).and_return(issue)
          expect(subject).to receive(:update_labels).twice

          expect { subject.invoke! }.to output(/Creating test case/).to_stdout
        end
      end

      context 'when an issue exists for a given test' do
        context 'when the issue is linked in the test case' do
          it 'finds the issue via the link in the test case and updates the issue' do
            expect(subject).to receive(:find_testcase).with(anything).and_return(testcase)
            expect(subject).to receive(:find_linked_results_issue_by_iid).with(testcase, anything).and_return(issue)
            expect(subject).to receive(:update_labels).with(testcase, anything).and_return(true)
            expect(subject).to receive(:update_labels).with(issue, anything, anything).and_return(true)

            expect { subject.invoke! }.to output(/Issue updated/).to_stdout
          end

          context 'when the issue title has been changed' do
            let(:new_issue) { Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title).new('http://issue.url', issue_labels, 0, 'issue', 'opened', 'Updated Title') }

            it 'updates the issue title' do
              expect(subject).to receive(:find_testcase).with(anything).and_return(testcase)
              expect(subject).to receive(:find_linked_results_issue_by_iid).with(anything, anything).and_return(new_issue)
              expect(subject).to receive(:update_issue_title).with(anything, anything, 'issue').and_return(new_issue)
              expect(subject).to receive(:update_labels).twice

              expect { subject.invoke! }.to output.to_stdout
            end

            %w[canary production preprod release].each do |pipeline|
              context "with a #{pipeline} pipeline" do
                around do |example|
                  ClimateControl.modify(
                    CI_PROJECT_NAME: pipeline,
                    TOP_UPSTREAM_SOURCE_JOB: 'https://ops.gitlab.net',
                    CI_COMMIT_REF_NAME: Gitlab::QA::Runtime::Env.default_branch) { example.run }
                end

                it 'does not update the issue title' do
                  expect(subject).to receive(:find_testcase).with(anything).and_return(testcase)
                  expect(subject).to receive(:find_linked_results_issue_by_iid).with(anything, anything).and_return(new_issue)
                  expect(subject).not_to receive(:update_issue_title)
                  expect(subject).to receive(:update_labels).twice

                  expect { subject.invoke! }.to output.to_stdout
                end
              end
            end
          end

          context 'when the issue is closed' do
            let(:closed_issue) { Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title).new('http://issue.url', issue_labels, 0, 'issue', 'closed', title) }

            it 'creates a new issue and updates the test case with the new issue link' do
              expect(subject).to receive(:find_testcase).with(anything).and_return(testcase)
              expect(subject).to receive(:issue_iid_from_testcase).with(anything).and_return(0)
              expect(::Gitlab).to receive(:issue).and_return(closed_issue)
              expect(subject).to receive(:find_issue).and_return(nil)
              expect(subject).to receive(:create_issue).and_return(issue)
              expect(::Gitlab).to receive(:edit_issue)
                .with(
                  project,
                  testcase.iid,
                  hash_including(description: anything))
                .and_return(testcase)
              expect(subject).to receive(:update_labels).twice

              expect { subject.invoke! }
                .to output(/No valid issue link found\nCreated new issue: #{issue.web_url}\nAdded issue #{issue.web_url} to testcase #{testcase.web_url}/).to_stdout
            end
          end
        end

        context 'when the issue is not linked in the test case' do
          let(:description) { "" }

          it 'finds the issue by searching by the test file and name and updates the test case with issue link' do
            expect(subject).to receive(:find_testcase).with(anything).and_return(testcase)
            expect(subject).to receive(:find_issue).with(anything, 'issue').and_return(issue)
            expect(::Gitlab).to receive(:edit_issue)
              .with(
                project,
                testcase.iid,
                hash_including(description: anything))
              .and_return(testcase)
            expect(subject).to receive(:update_labels).with(testcase, anything).and_return(true)
            expect(subject).to receive(:update_labels).with(issue, anything, anything).and_return(true)

            expect { subject.invoke! }.to output(/Found existing issue: #{issue.web_url}\nAdded issue #{issue.web_url} to testcase #{testcase.web_url}/).to_stdout
          end

          context 'when the test name makes the title longer than the maximum 255 character' do
            let(:long_test_name) { 'x' * 255 }
            let(:name_truncated_to_fit_title) { 'x' * 220 }
            let(:test_data) { %(<testcase name="#{long_test_name}" file="#{test_file_full}"/>) }
            let(:title) { "#{test_file_partial} | #{name_truncated_to_fit_title}..." }

            it 'finds the test case and issue with a truncated title' do
              expect(subject).to receive(:find_testcase).with(anything).and_return(testcase)
              expect(subject).to receive(:find_linked_results_issue_by_iid).with(testcase, anything).and_return(issue)
              expect(subject).to receive(:update_labels).twice

              expect { subject.invoke! }.to output.to_stdout
            end
          end
        end

        context 'when the results are in JSON format' do
          let(:testcase_url) { 'https://gitlab.com/gitlab-org/quality/testcases/-/quality/test_cases/1269' }
          let(:test_file) { 'file.json' }
          let(:test_data) do
            <<~JSON
              {
                "examples": [
                  {
                    "full_description": "test-name",
                    "file_path": "#{test_file_full}",
                    "testcase": "#{testcase_url}"
                  }
                ]
              }
            JSON
          end

          before do
            allow(::File).to receive(:write)
          end

          context 'when a test case is linked in the given test' do
            it 'finds the test case by given URL' do
              expect(subject).to receive(:find_testcase_by_iid).with(anything).and_return(testcase)
              expect(subject).to receive(:find_linked_results_issue_by_iid).and_return(issue)
              expect(subject).to receive(:update_labels).twice

              expect { subject.invoke! }.to output.to_stdout
            end

            context 'when the testcase title has been changed' do
              let(:new_testcase) { Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title).new('http://testcase.url', testcase_labels, 1, 'test_case', 'opened', 'Updated Title') }

              it 'updates the test case title' do
                expect(subject).to receive(:find_testcase_by_iid).with(anything).and_return(new_testcase)
                expect(subject).to receive(:update_issue_title).with(anything, anything, 'test_case').and_return(new_testcase)
                expect(subject).to receive(:find_linked_results_issue_by_iid).and_return(issue)
                expect(subject).to receive(:update_labels).twice

                expect { subject.invoke! }.to output.to_stdout
              end

              %w[canary production preprod release].each do |pipeline|
                context "with a #{pipeline} pipeline" do
                  around do |example|
                    ClimateControl.modify(
                      CI_PROJECT_NAME: pipeline,
                      TOP_UPSTREAM_SOURCE_JOB: 'https://ops.gitlab.net',
                      CI_COMMIT_REF_NAME: Gitlab::QA::Runtime::Env.default_branch) { example.run }
                  end

                  it 'does not update the test case title' do
                    expect(subject).to receive(:find_testcase_by_iid).with(anything).and_return(new_testcase)
                    expect(subject).not_to receive(:update_issue_title)
                    expect(subject).to receive(:find_linked_results_issue_by_iid).and_return(issue)
                    expect(subject).to receive(:update_labels).twice

                    expect { subject.invoke! }.to output.to_stdout
                  end
                end
              end
            end
          end

          context 'when there is no testcase' do
            let(:test_data) do
              <<~JSON
                {
                  "examples": [
                    {
                      "full_description": "test-name",
                      "file_path": "#{test_file_full}"
                    }
                  ]
                }
              JSON
            end

            it 'finds the test case and writes it back to the report' do
              expect(subject).to receive(:find_testcase).with(anything).and_return(testcase)
              expect(subject).to receive(:find_linked_results_issue_by_iid).with(testcase, anything).and_return(issue)
              expect(subject).to receive(:update_labels).with(testcase, anything).and_return(true)
              expect(subject).to receive(:update_labels).with(issue, anything, anything).and_return(true)

              test_data_parsed = JSON.parse(test_data)
              example = test_data_parsed.dig('examples', 0)
              example['testcase'] = testcase.web_url

              expect(::File).to receive(:write).with('file.json', JSON.pretty_generate(test_data_parsed))

              expect { subject.invoke! }.to output.to_stdout
            end
          end
        end
      end

      context 'when an issue does not exist for a given test' do
        before do
          allow(subject).to receive(:find_testcase).with(anything).and_return(testcase)
          allow(subject).to receive(:find_linked_results_issue_by_iid).with(testcase, anything).and_return(nil)
          allow(subject).to receive(:find_issue).and_return(nil)
        end

        it 'creates a new issue and adds the link to the test case' do
          expect(subject).to receive(:create_issue).and_return(issue)
          expect(::Gitlab).to receive(:edit_issue)
            .with(
              project,
              testcase.iid,
              hash_including(description: anything))
            .and_return(testcase)
          expect(subject).to receive(:update_labels).twice

          expect { subject.invoke! }
            .to output(/Created new issue: #{issue.web_url}\nAdded issue #{issue.web_url} to testcase #{testcase.web_url}/).to_stdout
        end

        context 'when creating a new issue' do
          it 'creates the issue in the provided project' do
            expect(::Gitlab).to receive(:create_issue)
              .with(
                project,
                "#{test_file_partial} | test-name",
                hash_including(description: "### Full description\n\ntest-name\n\n### File path\n\n#{test_file_full}", issue_type: 'issue', labels: testcase_labels))
              .and_return(issue)

            expect(subject).to receive(:add_issue_to_testcase).with(testcase, issue)
            expect(subject).to receive(:update_labels).twice

            expect { subject.invoke! }.to output.to_stdout
          end

          context 'with EE tests' do
            let(:test_file_full) { 'qa/specs/features/ee/browser_ui/stage/test_spec.rb' }

            it 'applies the ~"Enterprise Edition" label to test case and issue' do
              ClimateControl.modify(CI_PROJECT_NAME: 'staging') do
                allow(subject).to receive(:update_labels).and_call_original

                expect(subject).to receive(:create_issue).and_return(issue)
                expect(subject).to receive(:add_issue_to_testcase).with(testcase, issue)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    testcase.iid,
                    hash_including(labels: %w[Quality devops::stage status::automated Enterprise\ Edition staging::passed]))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    issue.iid,
                    hash_including(labels: %w[Quality devops::stage status::automated Testcase\ Linked Enterprise\ Edition staging::passed]))
                  .and_return(issue)

                expect { subject.invoke! }.to output.to_stdout
              end
            end

            it 'removes ee from the path in the title but not the description' do
              expect(::Gitlab).to receive(:create_issue)
                .with(anything,
                      "browser_ui/stage/test_spec.rb | test-name",
                      hash_including(description: "### Full description\n\ntest-name\n\n### File path\n\n#{test_file_full}"))
                .and_return(issue)

              expect(subject).to receive(:add_issue_to_testcase).with(testcase, issue)
              expect(subject).to receive(:update_labels).twice

              expect { subject.invoke! }.to output.to_stdout
            end
          end
        end
      end

      context 'with an existing or new issue' do
        before do
          allow(subject).to receive(:find_testcase).with(anything).and_return(testcase)
          allow(subject).to receive(:find_linked_results_issue_by_iid).with(testcase, anything).and_return(issue)
        end

        context 'with a passing test' do
          it 'adds a passed label' do
            ClimateControl.modify(CI_PROJECT_NAME: 'production') do
              expect(::Gitlab).to receive(:edit_issue)
                .with(
                  project,
                  testcase.iid,
                  hash_including(labels: testcase_labels + ["production::passed"]))
                .and_return(testcase)

              expect(::Gitlab).to receive(:edit_issue)
                .with(
                  project,
                  issue.iid,
                  hash_including(labels: issue_labels + ["production::passed"]))
                .and_return(issue)

              expect { subject.invoke! }.to output.to_stdout
            end
          end

          it 'does not add a note' do
            expect(::Gitlab).to receive(:edit_issue).and_return(testcase)
            expect(::Gitlab).to receive(:edit_issue).and_return(issue)
            expect(::Gitlab).not_to receive(:create_issue_note)

            expect { subject.invoke! }.to output.to_stdout
          end

          context 'with an existing failed label' do
            let(:testcase_labels) { ['Quality', 'devops::stage', 'status::automated', 'staging::failed'] }

            it 'replaces the label' do
              ClimateControl.modify(CI_PROJECT_NAME: 'staging') do
                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    testcase.iid,
                    hash_including(labels: ['Quality', 'devops::stage', 'status::automated', 'staging::passed']))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    issue.iid,
                    hash_including(labels: ['Quality', 'devops::stage', 'status::automated', 'Testcase Linked', 'staging::passed']))
                  .and_return(issue)

                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end
        end

        context 'with a failed test' do
          let(:test_data) { "<testcase name='test-name' file='#{test_file_full}'><failure message='An Error Here' type='Error'>Test Stacktrace</failure></testcase>" }

          before do
            allow(::Gitlab).to receive(:edit_issue).and_return(testcase)
            allow(::Gitlab).to receive(:edit_issue).and_return(issue)
          end

          it 'adds a failed label' do
            ClimateControl.modify(CI_PROJECT_NAME: 'production') do
              expect(::Gitlab).to receive(:edit_issue)
                .with(
                  project,
                  testcase.iid,
                  hash_including(labels: testcase_labels + ["production::failed"]))
                .and_return(testcase)

              expect(::Gitlab).to receive(:edit_issue)
                .with(
                  project,
                  issue.iid,
                  hash_including(labels: issue_labels + ["production::failed"]))
                .and_return(issue)

              expect(subject).to receive(:note_status)

              expect { subject.invoke! }.to output.to_stdout
            end
          end

          context 'when reporting for master/main pipelines' do
            around do |example|
              ClimateControl.modify(TOP_UPSTREAM_SOURCE_JOB: 'https://gitlab.com') { example.run }
            end

            it 'can report from gitlab-qa' do
              ClimateControl.modify(CI_PROJECT_NAME: 'gitlab-qa') do
                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    testcase.iid,
                    hash_including(labels: testcase_labels + ["#{Gitlab::QA::Runtime::Env.default_branch}::failed"]))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    issue.iid,
                    hash_including(labels: issue_labels + ["#{Gitlab::QA::Runtime::Env.default_branch}::failed"]))
                  .and_return(issue)

                expect(subject).to receive(:note_status)

                expect { subject.invoke! }.to output.to_stdout
              end
            end

            it 'can report from gitlab-qa-mirror' do
              ClimateControl.modify(CI_PROJECT_NAME: 'gitlab-qa-mirror') do
                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    testcase.iid,
                    hash_including(labels: testcase_labels + ["#{Gitlab::QA::Runtime::Env.default_branch}::failed"]))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    issue.iid,
                    hash_including(labels: issue_labels + ["#{Gitlab::QA::Runtime::Env.default_branch}::failed"]))
                  .and_return(issue)

                expect(subject).to receive(:note_status)

                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end

          context 'with an existing passed label' do
            let(:testcase_labels) { ['Quality', 'devops::stage', 'status::automated', 'staging::passed'] }

            it 'replaces the label' do
              ClimateControl.modify(CI_PROJECT_NAME: 'staging') do
                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    testcase.iid,
                    hash_including(labels: ['Quality', 'devops::stage', 'status::automated', 'staging::failed']))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    issue.iid,
                    hash_including(labels: ['Quality', 'devops::stage', 'status::automated', 'Testcase Linked', 'staging::failed']))
                  .and_return(issue)

                expect(subject).to receive(:note_status)

                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end

          context 'when reporting a specific job' do
            let(:failure_summary) { ":x: ~\"staging::failed\" in job `test-job` in http://job_url" }
            let(:note_content) { "#{failure_summary}\n\nError:\n```\nError: An Error Here\n```\n\nStacktrace:\n```\nTest Stacktrace\n```\n" }
            let(:paginated_response) { Struct.new(:auto_paginate) }

            around do |example|
              ClimateControl.modify(
                CI_JOB_URL: 'http://job_url',
                CI_JOB_NAME: 'test-job',
                CI_PROJECT_NAME: 'staging'
              ) { example.run }
            end

            it 'adds a note that the test failed and a stack trace' do
              expect(::Gitlab).to receive(:issue_discussions).and_return(paginated_response.new([]))
              expect(::Gitlab).to receive(:create_issue_note)
                .with(anything, anything, note_content)

              expect { subject.invoke! }.to output.to_stdout
            end

            context 'with an existing discussion' do
              let(:existing_discussion) { Struct.new(:notes, :id).new(['body' => note_content], 0) }

              it 'adds a note to the discussion with no stack trace' do
                expect(::Gitlab).to receive(:issue_discussions).and_return(paginated_response.new([existing_discussion]))
                expect(::Gitlab).to receive(:add_note_to_issue_discussion_as_thread)
                  .with('valid-project', 0, 0, body: failure_summary)

                expect { subject.invoke! }.to output.to_stdout
              end

              context 'when the error or stack trace do not match' do
                let(:existing_discussion) do
                  Struct.new(:notes, :id)
                        .new(['body' => "#{failure_summary}\n\nError:\n```\nError: This time it's different\n```\n\nStacktrace:\n```\nAlso different\n```\n"], 0)
                end

                it 'adds a note as a new discussion' do
                  expect(::Gitlab).to receive(:issue_discussions).and_return(paginated_response.new([existing_discussion]))
                  expect(::Gitlab).not_to receive(:add_note_to_issue_discussion_as_thread)
                  expect(::Gitlab).to receive(:create_issue_note)
                    .with(anything, anything, note_content)

                  expect { subject.invoke! }.to output.to_stdout
                end
              end

              context 'with a different job name and environment' do
                around do |example|
                  ClimateControl.modify(
                    CI_JOB_URL: 'http://job_url',
                    CI_JOB_NAME: 'different-test-job',
                    CI_PROJECT_NAME: 'production'
                  ) { example.run }
                end

                it 'still matches the error and stack trace' do
                  expect(::Gitlab).to receive(:issue_discussions).and_return(paginated_response.new([existing_discussion]))
                  expect(::Gitlab).to receive(:add_note_to_issue_discussion_as_thread)
                    .with('valid-project', 0, 0, body: ":x: ~\"production::failed\" in job `different-test-job` in http://job_url")

                  expect { subject.invoke! }.to output.to_stdout
                end
              end
            end

            context 'when the test is quarantined' do
              let(:failure_summary) { ":x: ~\"staging::failed\" ~\"quarantine\" in job `test-job-quarantine` in http://job_url" }

              around do |example|
                ClimateControl.modify(
                  CI_JOB_URL: 'http://job_url',
                  CI_JOB_NAME: 'test-job-quarantine'
                ) { example.run }
              end

              before do
                allow(::Gitlab).to receive(:issue_discussions).and_return(paginated_response.new([]))
              end

              it 'applies a quarantine label and includes the same in the summary' do
                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    testcase.iid,
                    hash_including(labels: testcase_labels + ['quarantine', 'staging::failed']))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    issue.iid,
                    hash_including(labels: issue_labels + ['quarantine', 'staging::failed']))
                  .and_return(issue)

                expect(::Gitlab).to receive(:create_issue_note)
                  .with(anything, anything, note_content)

                expect { subject.invoke! }.to output.to_stdout
              end

              context 'when the issue already has the same labels' do
                let(:testcase_labels) { %w[Quality devops::stage status::automated staging::failed quarantine] }

                it 'does not update the issue to change the labels' do
                  expect(::Gitlab).not_to receive(:edit_issue)
                  expect(::Gitlab).to receive(:create_issue_note)

                  expect { subject.invoke! }.to output.to_stdout
                end
              end
            end

            context 'when a quarantined test is dequarantined' do
              let(:testcase_labels) { %w[Quality devops::stage status::automated staging::failed quarantine] }
              let(:issue_labels) { %w[Quality devops::stage status::automated staging::failed quarantine Testcase\ Linked] }

              it 'removes the quarantine label' do
                allow(::Gitlab).to receive(:issue_discussions).and_return(paginated_response.new([]))

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    testcase.iid,
                    hash_including(labels: ['Quality', 'devops::stage', 'status::automated', 'staging::failed']))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    project,
                    issue.iid,
                    hash_including(labels: ['Quality', 'devops::stage', 'status::automated', 'Testcase Linked', 'staging::failed']))
                  .and_return(issue)

                expect(::Gitlab).to receive(:create_issue_note)
                  .with(anything, anything, note_content)

                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end
        end

        context 'when an error occurs' do
          let(:post_to_slack) { double('Gitlab::QA::Slack::PostToSlack') }

          before do
            allow(post_to_slack).to receive(:invoke!)
          end

          around do |example|
            ClimateControl.modify(
              CI_SLACK_WEBHOOK_URL: 'http://webhook_url',
              QA_DEFAULT_BRANCH: 'master') { example.run }
          end

          %w[staging production preprod nightly release].each do |pipeline|
            context "with a #{pipeline} pipeline" do
              around do |example|
                ClimateControl.modify(
                  CI_PROJECT_NAME: pipeline,
                  TOP_UPSTREAM_SOURCE_JOB: 'https://ops.gitlab.net',
                  CI_COMMIT_REF_NAME: Gitlab::QA::Runtime::Env.default_branch) { example.run }
              end

              it "posts a Slack message to qa-#{pipeline}" do
                allow(::Gitlab).to receive(:send).and_raise(StandardError)

                expect(Gitlab::QA::Slack::PostToSlack).to receive(:new).with(hash_including(channel: "qa-#{pipeline}")).and_return(post_to_slack).twice
                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end

          %w[gitlab-qa gitlab-qa-mirror].each do |pipeline|
            context "with a #{pipeline} pipeline" do
              around do |example|
                ClimateControl.modify(
                  CI_PROJECT_NAME: pipeline,
                  TOP_UPSTREAM_SOURCE_JOB: 'https://gitlab.com',
                  CI_COMMIT_REF_NAME: Gitlab::QA::Runtime::Env.default_branch) { example.run }
              end

              it "posts a Slack message to qa-master" do
                allow(::Gitlab).to receive(:send).and_raise(StandardError)

                expect(Gitlab::QA::Slack::PostToSlack).to receive(:new).with(hash_including(channel: "qa-master")).and_return(post_to_slack).twice
                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end

          context 'with a canary pipeline' do
            around do |example|
              ClimateControl.modify(CI_PROJECT_NAME: 'canary', CI_COMMIT_REF_NAME: Gitlab::QA::Runtime::Env.default_branch) { example.run }
            end

            it 'posts a Slack message to qa-production' do
              allow(::Gitlab).to receive(:send).and_raise(StandardError)

              expect(Gitlab::QA::Slack::PostToSlack).to receive(:new).with(hash_including(channel: "qa-production")).and_return(post_to_slack).twice
              expect { subject.invoke! }.to output.to_stdout
            end
          end

          context 'with a staging-canary pipeline' do
            around do |example|
              ClimateControl.modify(CI_PROJECT_NAME: 'staging-canary', CI_COMMIT_REF_NAME: Gitlab::QA::Runtime::Env.default_branch) { example.run }
            end

            it 'posts a Slack message to qa-staging' do
              allow(::Gitlab).to receive(:send).and_raise(StandardError)

              expect(Gitlab::QA::Slack::PostToSlack).to receive(:new).with(hash_including(channel: "qa-staging")).and_return(post_to_slack).twice
              expect { subject.invoke! }.to output.to_stdout
            end
          end

          context 'when ref is not master' do
            around do |example|
              ClimateControl.modify(CI_COMMIT_REF_NAME: 'feature-branch') { example.run }
            end

            it 'reports the error and terminates without posting to Slack' do
              allow(::Gitlab).to receive(:send).and_raise(StandardError)

              expect(Gitlab::QA::Slack::PostToSlack).not_to receive(:new)
              expect { subject.invoke! }.to output.to_stdout
            end
          end

          context 'when a user permission error occurs' do
            it 'reports the error and terminates without posting to Slack' do
              stub_const("Gitlab::Error::NotFound", RuntimeError)

              allow(subject.__send__(:gitlab)).to receive(:assert_user_permission!).and_call_original
              allow(::Gitlab).to receive(:user).and_raise(Gitlab::Error::NotFound)

              expect(subject).not_to receive(:report_test)
              expect(Gitlab::QA::Slack::PostToSlack).not_to receive(:new)
              expect { subject.invoke! }
                .to output("You must have at least Maintainer access to the project to use this feature.\n").to_stderr
                .and raise_error(SystemExit)
            end
          end

          context 'when a timeout error occurs' do
            it 'retries up to 3 times before letting the error fail the job' do
              stub_const("Gitlab::QA::Report::GitlabIssueClient::RETRY_BACK_OFF_DELAY", 0.000001)

              allow(::Gitlab).to receive(:send).and_raise(Errno::ETIMEDOUT)

              expect(Gitlab::QA::Slack::PostToSlack).not_to receive(:new)

              expect { subject.invoke! }
                .to output(/Sleeping for .* seconds before retrying.../).to_stderr
                .and raise_error(Errno::ETIMEDOUT)
            end
          end
        end
      end
    end
  end
end
