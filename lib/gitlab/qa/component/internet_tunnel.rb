require 'tempfile'

module Gitlab
  module QA
    module Component
      class InternetTunnel < Base
        DOCKER_IMAGE = 'gitlab/ssh-tunnel'.freeze
        DOCKER_IMAGE_TAG = '1.0.0'.freeze

        attr_writer :gitlab_hostname

        def initialize
          super

          key_dir = ENV['CI_PROJECT_DIR'] || Dir.tmpdir
          @ssh_key = Tempfile.new('tunnel-ssh-private-key', key_dir)
          @ssh_key.write(ENV.fetch('TUNNEL_SSH_PRIVATE_KEY'))
          @ssh_key.close

          File.chmod(0o600, @ssh_key.path)

          @volumes['/root/.ssh/id_rsa'] = @ssh_key.path
        end

        def instance
          raise 'Please provide a block!' unless block_given?

          super
        end

        def url
          "https://#{subdomain}.#{tunnel_server_hostname}"
        end

        private

        def name
          @name ||= "ssh-tunnel-#{SecureRandom.hex(4)}"
        end

        def tunnel_server_hostname
          ENV.fetch("TUNNEL_SERVER_HOSTNAME")
        end

        def subdomain
          @subdomain ||= rand(30_000..49_000)
        end

        def start
          raise "Must set gitlab_hostname" unless @gitlab_hostname

          @docker.run(
            image: DOCKER_IMAGE,
            tag: DOCKER_IMAGE_TAG,
            args: ["-o StrictHostKeyChecking=no -N -R #{subdomain}:#{@gitlab_hostname}:80 #{ENV.fetch('TUNNEL_SSH_USER')}@#{tunnel_server_hostname}"]) do |command|
            command << '-d '
            command << "--name #{name}"
            command << "--net #{network}"

            @volumes.to_h.each do |to, from|
              command.volume(from, to, 'Z')
            end
          end
        end

        def teardown
          super

          @ssh_key.unlink
        end
      end
    end
  end
end
